# MITIGA - Android App Review

## **0.0.Disclaimer**

_This is just a a quick review of a mobile app, that has been publicly released on the official App Stores for iOS & Android devices._
 
_All the information are only for the sake of providing useful feedbacks to the developers of such app, in order to help them better identify and solve the reported issues._
 
_All checks have been carried out without any alteration of the app code, just using the current official releases of the app._
 
_If you want to report any further issues and/or request official support about such app, please get in touch with the app developers._
 
.
.

## **0.1.What is Mitiga ?**

According to the description you can read on the [official website](https://www.mitigaitalia.it/) :

> Mitiga is a platform that allows in a quick and standardized way to document the COVID status of the citizen (self-declaration of the state of health, antigen test for Sars-Cov 2, vaccine)
> 
> Mitiga's purpose is to allow access to stadiums and entertainment venues only to people who have had the vaccine or who have tested negative for a Sars-Cov 2 antigen test carried out at an authorized center, in a pre-established time frame prior to the event.
> The authorization to enter takes place in a totally standardized and secure manner using an identification QR Code, without detecting additional sensitive information.

Such platform includes :

- Restricted-access webportal - to be used by pharmacies and laboratories, that carry out Sars-Cov2 tests or vaccines ([preview](https://lab.mitigaitalia.it)) 

- Scan app - to be used by event managers (_[currently available for iOS only](https://apps.apple.com/it/app/mitiga-scan/id1560203836)_)

- User app

The user app has been initially released - at the end of January 2021 - for iOS devices only (App Store). It's available for Android devices too on the Huawei AppGallery (since April 9th) and on the Google PlayStore (since May 5th). 

| Build    | OS      | Store      | Link                                              |
|----------|---------|------------|---------------------------------------------------|  
| 1.4.3    | iOS     | Apple App Store  | https://apps.apple.com/it/app/mitiga/id1546762246 |
| 1.4.3.67 | Android | Huawei AppGallery | https://appgallery.huawei.com/#/app/C104156933    |
| 1.4.2.63 | Android | Google Play Store | https://play.google.com/store/apps/details?id=it.mitigaitalia.android |

.

The AppGallery releases aren't strictly HMS/GMS-dependant & have been published - until now - as single/universal APKs (_not as bundle packages_). Therefore, the users can also install them through APK sideload on any Android device.

The PlayStore releases are GMS-dependant & have been published as bundle packages. Therefore, PlayStore builds can be installed only on devices with GMS support (Google Play services or MicroG), through Google PlayStore or Split APK installers (_together with the specific language/dpi/arm config APKs - compatible with the user's device_).

> Online reports for **current AppGallery Android release 1.4.3.67**

| Site | Link |
|------|------|
| VirusTotal | https://www.virustotal.com/gui/file/be9b4f4303808a99b2398ba481b0b27feb5cae53f4c290600d5ce0dbca2caab5/details |
| ApkLab | https://apklab.io/apk.html?hash=be9b4f4303808a99b2398ba481b0b27feb5cae53f4c290600d5ce0dbca2caab5 |
| JoeSandbox | https://www.joesandbox.com/analysis/419672/0/html |
| Triage | https://tria.ge/210521-hknwca5lsj |
| Pithus | https://beta.pithus.org/report/be9b4f4303808a99b2398ba481b0b27feb5cae53f4c290600d5ce0dbca2caab5 |

> Online reports for **current PlayStore Android release 1.4.2.63**

| Site | Link |
|------|------|
| VirusTotal | https://www.virustotal.com/gui/file/0930e5c8a1de1a2fb7ccbe8c2e212dbfd3f774a92f5b598262cfd186f5ef7dbc/details |
| ApkLab | https://apklab.io/apk.html?hash=0930e5c8a1de1a2fb7ccbe8c2e212dbfd3f774a92f5b598262cfd186f5ef7dbc |
| JoeSandbox | https://www.joesandbox.com/analysis/416881/0/html |
| Triage | https://tria.ge/210519-4xcc1snern |
| Pithus | https://beta.pithus.org/report/0930e5c8a1de1a2fb7ccbe8c2e212dbfd3f774a92f5b598262cfd186f5ef7dbc |

> Online reports for former AppGallery Android release 1.4.1.61

| Site | Link |
|------|------|
| VirusTotal | https://www.virustotal.com/gui/file/f5afdeaed1c08c74999f4782d5f2e99b53262bd45b97374f26919d57a0d1556e/details |
| ApkLab | https://apklab.io/apk.html?hash=f5afdeaed1c08c74999f4782d5f2e99b53262bd45b97374f26919d57a0d1556e |
| JoeSandbox | https://www.joesandbox.com/analysis/415839/0/html |
| Triage | https://tria.ge/210517-vpdn994dzj |
| Pithus | https://beta.pithus.org/report/f5afdeaed1c08c74999f4782d5f2e99b53262bd45b97374f26919d57a0d1556e |

> Online reports for former AppGallery Android release 1.3.1.58

| Site | Link |
|------|------|
| VirusTotal | https://www.virustotal.com/gui/file/c16072f0f918212156ed0b753e578c86f7b2cb1be671d09772d2cbcbd7e0a4ad/details |
| ApkLab | https://apklab.io/apk.html?hash=c16072f0f918212156ed0b753e578c86f7b2cb1be671d09772d2cbcbd7e0a4ad |
| JoeSandbox | https://www.joesandbox.com/analysis/414149/0/html |
| Triage | https://tria.ge/210514-whe7z2wzw6 |
| Pithus | https://beta.pithus.org/report/c16072f0f918212156ed0b753e578c86f7b2cb1be671d09772d2cbcbd7e0a4ad |

> Online reports for former PlayStore Android release 1.3.0.56

| Site | Link |
|------|------|
| VirusTotal | https://www.virustotal.com/gui/file/5b004b47264c4523d90f5cb54c2a9f6ce35fabaaad0575c92f299007a424fff7/details |
| ApkLab | https://apklab.io/apk.html?hash=5b004b47264c4523d90f5cb54c2a9f6ce35fabaaad0575c92f299007a424fff7 |
| JoeSandbox | https://www.joesandbox.com/analysis/413326/0/html |
| Triage | https://tria.ge/210513-vwhevx75p2 |
| Pithus | https://beta.pithus.org/report/5b004b47264c4523d90f5cb54c2a9f6ce35fabaaad0575c92f299007a424fff7 |

> Online reports for former PlayStore Android release 1.2.2.54

| Site | Link |
|------|------|
| VirusTotal | https://www.virustotal.com/gui/file/d73c2a0b5445b500fe06bbc9de712776dca04be6316407314989af1b293c874c/details |
| ApkLab | https://apklab.io/apk.html?hash=d73c2a0b5445b500fe06bbc9de712776dca04be6316407314989af1b293c874c |
| JoeSandbox | https://www.joesandbox.com/analysis/410964/0/html |
| Triage | https://tria.ge/210511-n2cretbjsj |
| Pithus | https://beta.pithus.org/report/d73c2a0b5445b500fe06bbc9de712776dca04be6316407314989af1b293c874c |

> Online reports for former PlayStore Android release 1.1.2.51

| Site | Link |
|------|------|
| VirusTotal | https://www.virustotal.com/gui/file/74bf1c1f4ef3e1ff54edb05f0ce13ecd823e1300abd7d797dd0a1101c1b78f6c/details |
| ApkLab | https://apklab.io/apk.html?hash=74bf1c1f4ef3e1ff54edb05f0ce13ecd823e1300abd7d797dd0a1101c1b78f6c |
| JoeSandbox | https://www.joesandbox.com/analysis/405970/0/html |
| Triage | https://tria.ge/210506-32yv2tdrvx |
| Pithus | https://beta.pithus.org/report/74bf1c1f4ef3e1ff54edb05f0ce13ecd823e1300abd7d797dd0a1101c1b78f6c |

> Online reports for former Appgallery Android release 1.1.0.48

| Site | Link |
|------|------|
| VirusTotal | https://www.virustotal.com/gui/file/178e33d4234ab921e08ce71939f0af2e6f5381129d30082ad9d2a2cfc882bb7c/details |
| ApkLab | https://apklab.io/apk.html?hash=178e33d4234ab921e08ce71939f0af2e6f5381129d30082ad9d2a2cfc882bb7c |
| JoeSandbox | https://www.joesandbox.com/analysis/403959/0/html |
| Triage | https://tria.ge/210504-atgftsejba |
| Pithus | https://beta.pithus.org/report/178e33d4234ab921e08ce71939f0af2e6f5381129d30082ad9d2a2cfc882bb7c |

> Online reports for former AppGallery Android release 1.0.6.44

| Site | Link |
|------|------|
| VirusTotal | https://www.virustotal.com/gui/file/5734973cb524e35d00aa2d551cd9febf78f4826a91db6f92b1b7442db0356f1c/details |
| ApkLab | https://apklab.io/apk.html?hash=5734973cb524e35d00aa2d551cd9febf78f4826a91db6f92b1b7442db0356f1c |
| JoeSandbox | https://www.joesandbox.com/analysis/384682/0/html |
| Triage | https://tria.ge/210409-hx6lqmfrp6 |
| Pithus | https://beta.pithus.org/report/5734973cb524e35d00aa2d551cd9febf78f4826a91db6f92b1b7442db0356f1c |

.
.

## 1.First launch & Account Registration

Notice : PlayStore release 1.1.2.51 had an installation bug. Even though it had been installed right (1 single entry into Settings - Apps), the users were finding 2 related icons in the app drawer.

The first icon wasn't working = users had to tap on the 2nd icon in order to launch the app...

| Settings - Apps | App Drawer bug 1.1.2.51 |
|------------------|---------------------------|
| ![alt text](screenshots/0_otherbugs/OBUGS_01.png "Settings - App") | ![alt text](screenshots/0_otherbugs/OBUGS_02.png "App Drawer Bug") |

**Such setup bug has been fixed in PlayStore release 1.2.2.54**.

When the user launches the app for the first time, there are 3 welcome screens.

| Screen 1 | Screen 2 | Screen 3 |
| ------ | ------ | ------ |
| ![alt text](screenshots/0_registration/REG_00.png "Welcome Screen 1") | ![alt text](screenshots/0_registration/REG_01.png "Welcome Screen 2") | ![alt text](screenshots/0_registration/REG_02.png "Welcome Screen 3") |

.

Meanwhile the app performs 2 remote requests :

| Host | api.appcelerator.com |
|------|----------------------|
| Method | POST |
| Path | /p/v4/mobile-track |
| Purpose | Analytics + Device Registration |

| Host | api.mitigalia.it |
|------|----------------------|
| Method | GET |
| Path | /faq |
| Purpose | JSON retrieval of the latest FAQ update (_[at the present time - January 25, 2021](https://api.mitigaitalia.it/faq)_) |

.

After these screens, the user views a register/login screen, where he also finds the links to the in-app FAQ page, together with the external ones for the [Terms and Conditions](https://www.mitigaitalia.it/app-terms) and the [Privacy Policy](https://www.mitigaitalia.it/app-privacy).

| Register / Login |
|------------------|
| ![alt text](screenshots/0_registration/REG_03.png "Register / Login") |

.

The signup process requires the user to fill in a form. These are the required data :

- Name

- Surname

- **Email address**

- **Password**

- Country Prefix + Mobile Phone Number

- Citizenship (_Italy or other_)

- **Tax Code** (_requested to Italian citizens only_)

- Place of residence (_Italian province or abroad_)

Only the fields in bold are locally checked against in-app form rules. The user app performs a remote verification only for the e-mail address.

| Host | api.mitigaitalia.it |
|------|----------------------|
| Method | POST |
| Path | /users/email-availability |
| Purpose | To check that the address hasn't already been registered on the platform |

.

**Until release 1.0.6 the registration panel had a blocking UX/UI issue = page-scrolling hadn't been set right by the developers.**

Therefore, in such release the registration form could be fully filled out and submitted, only if/when the user managed to get it fully displayed onscreen ... and this was possible only under particular conditions, such as combining :

- QHD 1440x2560 resolution

- 640 density (XXXHDPI)

- The smallest size settings for onscreen fonts/elements

Even FHD 1080x1920 resolution wasn't enough with release 1.0.6 to fully display such form onscreen without the need to scroll.

**Such issue has been solved in the latest release 1.1.0.48 (May 4th, 2021) for Android.**
.

These are the registration forms, depending on whether the user selects Italian or Foreign Citizenship.

| Non-Italian Citizen / Tourist signup | Italian Citizen signup |
|----------------------------| -----------------------|
| ![alt text](screenshots/0_registration/REG_04.jpg "Non-Italian Citizen signup") | ![alt text](screenshots/0_registration/REG_05.jpg "Italian Citizen signup") | 

Some notes :

- There is no validation of the mobile phone number. It gets accepted even if it has already been submitted in former registrations.

- The tax code appears only by choosing Italian citizenship. Resident foreigners don't see such field.

- The form checks only the syntax of the tax code. It doesn't check whether it partially matches with the surname+name or whether it's a real tax code (_according to the Italian Revenue Agency_). Until 1.3.x releases there was no remote validation for the tax code at all = the apps weren't checking whether it had already been submitted in former registrations. **Only 1.3.x & subsequent PlayStore + AppGallery releases check if there is a former uuid / IT tax code registration & only when the user confirms the data submission from the summary page**.

- The Privacy/Agreement consent isn't fully GDPR compliant = the user is compelled to accept the data processing also for promotional/marketing purposes. **Only 1.4.3 releases have a new registration form with disjoint agreements for app usage & (optional) promotional/marketing purposes. However, the new registration form isn't working (KO on summary stage).**

| New Privacy Agreements - Only 1.4.3 Releases |
|-----------------------------------------|
| ![alt text](screenshots/0_registration/REG_07a.png " New Privacy Agreements - Only 1.4.3 Releases") |
.

Once the user agrees to the Privacy Policy & goes on, there is the photo-upload step. 

There are some warnings about the right photos to choose, but no face check/validation at all. After granting the storage permission, the user can upload [any image](https://files.mitigaitalia.it/avatars/340ab72058d44498fde6e176f06f11bcc3b4ee495f0291a4.jpeg).

| Photo Upload |
|--------------|
| ![alt text](screenshots/0_registration/REG_06.jpg "Photo Upload") |

.

In the end, there is a summary page before confirming the form-submission.

Here the user faced another bug - **fixed since release 1.2.2.54 on PlayStore**. **The country digit remained +39 (Italy), no matter which prefix he had selected in the former page**. 

It wasn't just a cosmetic bug = the form just wasn't changing at all the default value for the phonePrefix field. 

| Summary Page |
|--------------|
| ![alt text](screenshots/0_registration/REG_07.jpg "Summary Page") |

By pressing the register button, the app submits the related post request with the following form fields :

| Form Fields | Notes |
|-------------|-------|
| **profilePhoto** | avatar URL |
| **nationality** | IT or not |
| **firstName** | |
| **password** | cleartext / not hashed |
| **consent** | usage privacy agreement - value 1 (true in 1.4.3) |
| **consentNewsletter** | **only 1.4.3 builds** - mktg privacy agreement - values false/true |
| **uuid** | tax code for IT citizens - generated sequence for foreigners |
| **phonePrefix** | default value +39 |
| **lang** | system language of the device |
| **email** | |
| **lastName** | |
| **state** | Italian province or XX for not IT resident |
| **phoneNumber** | |


| Host | api.mitigaitalia.it |
|------|----------------------|
| Method | POST |
| Path | /users/register |
| Purpose | To submit the form fields |

**Since PlayStore release 1.3.0.56 such post request also performs a remote check of the uuid value = if it detects that a IT tax code has already been registered, it will return a warning message**.

| uuid - IT Tax Code already registered |
|--------------|
| ![alt text](screenshots/0_registration/REG_07b.jpg "IT tax code already registered") |

**1.4.3 releases KO submission bug : users of such releases can't submit the registration form because of a wrong value-type definition for consent & consentNewsletter fields**.

| 1.4.3 releases KO submission bug |
|----------------------------------|
| ![alt text](screenshots/0_registration/REG_07aa.png "1.4.3 releases KO submission bug") |

Once the form has been submitted OK, the user still has to activate the account.

| Confirmed submission |
|----------------------|
| ![alt text](screenshots/0_registration/REG_07c.jpg "Confirmed submission") |

.

The user receives indeed an email message - _delivered by the [Sendinblue](https://sendinblue.com) platform (according to IP Address / Message-ID / List-Unsubscribe header)_ - with a confirmation link in order to activate the account.

| Confirmation Request | Activation Page  |
|----------------------------| -----------------------|
| ![alt text](screenshots/0_registration/REG_08.png "Confirmation Request") | ![alt text](screenshots/0_registration/REG_09.png "Activation Page") | 

Now the account has been confirmed & activated.

.
.

## 2.First login & App usage

Now the user can login to the platform.

| Main Page | Login Page  |
|----------------------------| -----------------------|
| ![alt text](screenshots/1_login/LOG_01.png "Main Page") | ![alt text](screenshots/1_login/LOG_02.png "Login Page") | 

By pressing the login button, the app submits indeed email+password through an authentication request

| Host | api.mitigalia.it |
|------|----------------------|
| Method | POST |
| Path | /users/authenticate |
| Purpose | Retrieval of the phone number + Auth token |

.

Then there is a new login page where the user is solicited to request an OTP code via SMS, in order to confirm his mobile phone number too.

| OTP Page |
|----------|
| ![alt text](screenshots/1_login/LOG_03.png "OTP Page") |

Here you can see the side-effect of the country digit bug in the registration form - **fixed only since release 1.2.2.54 on PlayStore**.

.

If the user had registered an Italian number, there were no issues even with older Android releases. Otherwise, with such releases the user had to click on the green cross and add again the mobile phone number with the right international prefix.

In such case the user would have received another email message (delivered again by Sendinblue platform) with another confirmation link in order to add it.

| Add Phone Number | Phone Number Confirmation |
|------------------|---------------------------|
| ![alt text](screenshots/1_login/LOG_04.png "Add Phone Number") | ![alt text](screenshots/1_login/LOG_05.png "Phone Number Confirmation")

It wasn't a big issue, since the users can register up to 3 phone numbers (even already registered by other accounts), but it was quite annoying the same.

| OTP Page Multiple Phone Numbers |
|----------|
| ![alt text](screenshots/1_login/LOG_06.png "OTP Page Multiple Phone Numbers") |

**Even in the latest Android releases - PlayStore 1.3.0.56 & AppGallery 1.3.1.58 - there is NO remote validation for the 2 related OTP requests** = the OTP check isn't compliant yet (the PSD2 notice is quite misleading) & still can be passed even with fake/unexistent phone numbers.


| Host | api.mitigalia.it |
|------|----------------------|
| Method | POST |
| Path | /users/send-otp |
| Path | /users/check-otp |

In the latest PlayStore & AppGallery releases there was a bug related to the Health Declaration Duration status = once locally checked the OTP code, the app performed anyway the related check even for profiles that don't have 'true' values for the Validated status. **Fixed since 1.4.2.63 release**.

| Health Declaration Duration warning bug |
|----------|
| ![alt text](screenshots/1_login/LOG_03b.jpg "Health Declaration Duration warning bug") |

.

Once the number has been confirmed, the user enables the app notifications and (finally) can see the main panel.

| Enable Notifications | Main Panel |
|------------------|---------------------------|
| ![alt text](screenshots/1_login/LOG_07.png "Enable Notifications") | ![alt text](screenshots/1_login/LOG_08.png "Main Panel")

And that's it. The user just reads that the profile needs to be validated by going to an authorized centre.

**Update** : Since May 15th API responses are now returning true for the enableMap field. Therefore, also non-validated profiles are now allowed to locate in-app the authorized centres - ([list/map of labs](https://mitigaitalia.it/labs)). 

| Enabled Map | Authorized Centers |
|------------------|---------------------------|
| ![alt text](screenshots/1_login/LOG_08b.png "Enabled Map") | ![alt text](screenshots/1_login/LOG_08c.png "Authorized Centers")

Server-side map service (_host : api.mitigaitalia.it - path : /map_) now relies on [Mapbox](https://www.mapbox.com/), and no longer on Google Maps. However, at the present time it's working OK only on AppGallery releases.

**When the remote service detects the incoming client-connection from a PlayStore release - even the latest one 1.4.2.63 - it still performs former Google Maps call, that fails**.

**In such case the server returns indeed an HTTP 500 Internal Server Error because of Google Maps status REQUEST_DENIED (Not enabled billing for GCP project).**

**Therefore, users of PlayStore releases can't display map & markers. If they have granted the location permissions to the app, they can see only the list of the centers nearby.**

| KO PlayStore Map |
|------------------|
| ![alt text](screenshots/1_login/LOG_08d.jpg "KO PlayStore Map") | 

.
Obviously, the user can also grant the camera and storage permissions & launch the QR Code scanner. However, it's quite useless without any Mitiga codes to scan.

Besides, once a QR code has been scanned by user/scan app, it gets revoked = no longer valid.

This is for instance the scan-sequence with a test QR Code.

| Test QR Code | First scan | Further scans |
|--------------|------------|---------------|
| ![alt text](screenshots/1_login/LOG_08qr0.png "Test QR Code") | ![alt text](screenshots/1_login/LOG_08qr1.png "First scan") | ![alt text](screenshots/1_login/LOG_08qr2.png "Further scans") |

Such test QR code has already been revoked after its first scan - as also confirmed by the related [profileQR URL](https://api.mitigaitalia.it/profile/my4wnzw7jjdp50umfpw31jxz9l9vtcfi) .


.

The profile panel just summarizes the same data, that the user has submitted in the registration form. The only addition is such Mitiga Person ID (UUID), which is :

- Tax Code for Italian citizens

- Alphabetical code with ABC-DEFG-HIJ structure for foreigners

_**Note:** in the case of Italian citizens, the UUID is not an unique ID, since the app allows to register multiple accounts with different name + surname, but the same tax code._

| Foreigner | Italian Citizen |
|------------------|---------------------------|
| ![alt text](screenshots/1_login/LOG_09.png "Foreigner") | ![alt text](screenshots/1_login/LOG_10.png "Italian Citizen")

Since releases 1.3.x from the profile panel the user could link also the data of a relative... however such feature was quite buggy.

Vertical scrolling of the page was enabled by the developers. Therefore, the relative form could be filled only on devices with high resolution/DPI & by selectiong the smallest size settings for onscreen fonts/elements

Besides, the form got submitted OK only if the relative-data were those of an Italian citizen = there was a response error when selecting foreigner / No IT tax code.

| Add Relative form | Submission Confirmation |
|------------------|---------------------------|
| ![alt text](screenshots/1_login/LOG_10b.jpg "Add Relative form") | ![alt text](screenshots/1_login/LOG_10c.jpg "Submission Confirmation")

**Therefore, since 1.4.3 releases the 'add a relative' form has been fully disabled = those users, who want to link relatives to their profiles, must send an email message**.

| Disabled Relative form |
|------------------------|
| ![alt text](screenshots/1_login/LOG_10d.png "Disabled Relative form") |

.

Finally, there is the Settings panel, which can be used either to add other phone numbers or to send the account removal request.

| Settings |
|----------|
| ![alt text](screenshots/1_login/LOG_11.png "Settings") |

.
.

## **3.Some extra stuff**

Once the account activation procedure has been fulfilled, the user realizes that - _at the present time_ - even the latest Mitiga releases are still (buggy) demo-apps.

However, since it's a demo-app without the basic remote validation checks, it's quite easy to _persuade_ it to unlock those features - available only to the verified profiles :

- Swab panel 

- Authorized Centers locator (_requires location permission_) - **available since May 15th also to non-validated profiles**

- QR Code / Health Declaration page 

.

| Form | Summary |
|------------------|---------------------------|
| ![alt text](screenshots/2_extra/XTRA_01.jpg "Form") | ![alt text](screenshots/2_extra/XTRA_02.jpg "Summary") |

.

| Main Page | Authorized Centre Locator |
|------------------|---------------------------|
| ![alt text](screenshots/2_extra/XTRA_04.png "Main Page") | ![alt text](screenshots/2_extra/XTRA_05.png "Locator") |

.

| Profile | Phone Number Settings |
|------------------|---------------------------|
| ![alt text](screenshots/2_extra/XTRA_06.png "Profile") | ![alt text](screenshots/2_extra/XTRA_07.png "Phone Number") |

| QR Code show | QR Code / Health Declaration page |
|------------------|---------------------------|
| ![alt text](screenshots/2_extra/XTRA_08.png "QR Code Show") | ![alt text](screenshots/2_extra/XTRA_09.png "QR Code Page") |
.
.

## **4.Conclusions**

Technically speaking, until May 15th it wouldn't have been fair to give a grade/rating to such app.

Until such date it has been just a demo app & until 1.3.x releases the developers hadn't deployed any measure at all in order to control & validate the data exchange between the app and the remote endpoints.

.

However, even though now it can be evaluated as a full-fledged production app, the current rating of Mitiga app is still negative.

It's still buggy and the current server-side SSPI checks against used certificates aren't enough yet. OTP & some profile validation checks are still performed only locally on the client-endpoints. 

Besides, there are still privilege issues related to the session tokens and the data-processing isn't fully GDPR compliant yet. Only since latest 1.4.3 releases the users can oppose the processing of their data for promotional / marketing purposes. However, the new users of such releases can't submit their registration because of value-type definition bugs. Besides, it doesn't change the situation of formerly registered users, since they were compelled to agree also with the promotional/marketing purposes (single consent checkbox).
